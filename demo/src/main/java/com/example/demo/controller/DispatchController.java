package com.example.demo.controller;

import com.example.demo.entity.Dispatch;
import com.example.demo.service.DispatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/dispatch")
@CrossOrigin(origins = "http://localhost:4200")
public class DispatchController {
    private final DispatchService dispatchService;

    @Autowired
    public DispatchController(DispatchService dispatchService){
        this.dispatchService = dispatchService;
    }

    @GetMapping
    public List<Dispatch> getDispatch(){
        return dispatchService.getDispatch();
    }

    @GetMapping("/{dispatchId}")
    public Optional<Dispatch> getDispatch(@PathVariable("dispatchId") Long dispatchId){
        return dispatchService.getDispatch(dispatchId);
    }

    @PostMapping
    public ResponseEntity<Object> registerDispatch(@RequestBody Dispatch dispatch){
        return this.dispatchService.newDispatch(dispatch);
    }

    @PutMapping
    public ResponseEntity<Object> updateDispatch(@RequestBody Dispatch dispatch){
        return this.dispatchService.newDispatch(dispatch);
    }

    @DeleteMapping("{dispatchId}")
    public ResponseEntity<Object> deleteDispatch(@PathVariable("dispatchId") Long dispatchId){
        return this.dispatchService.deleteDispatch(dispatchId);
    }
}
