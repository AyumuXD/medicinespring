package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "UserType")
public class UserType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userTypeId;
    private String typeName;

    @OneToMany(mappedBy = "userType")
    @JsonManagedReference
    private List<User> user;
    public UserType() {
    }

    public UserType(Long userTypeId, String typeName, List<User> user) {
        this.userTypeId = userTypeId;
        this.typeName = typeName;
        this.user = user;
    }

    public UserType(String typeName, List<User> user) {
        this.typeName = typeName;
        this.user = user;
    }

    public Long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Long userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }
}
