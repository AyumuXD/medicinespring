package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "UserPlan")
public class UserPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userPlanId;
    private String planName;

    @OneToMany(mappedBy = "userPlan")
    @JsonManagedReference
    private List<User> user;
    public UserPlan() {
    }

    public UserPlan(Long userPlanId, String planName, List<User> user) {
        this.userPlanId = userPlanId;
        this.planName = planName;
        this.user = user;
    }

    public UserPlan(String planName, List<User> user) {
        this.planName = planName;
        this.user = user;
    }


    public Long getUserPlanId() {
        return userPlanId;
    }

    public void setUserPlanId(Long userPlanId) {
        this.userPlanId = userPlanId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String PlanName) {
        this.planName = PlanName;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }
}
